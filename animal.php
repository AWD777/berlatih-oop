<?php
//Terdapat sebuah class Animal yang memiliki sebuah constructor name, default property legs = 4 dan cold_blooded = no.
class Animal{
    public $name;
    public $legs=4;
    public $cold_blooded='no';

    public function __construct($stringtext){
        $this->name=$stringtext;
    }
}
?>