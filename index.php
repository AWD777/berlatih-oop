<?php
    require_once('animal.php');
    require_once('Frog.php');
    require_once('Ape.php');
    echo '<h3>Release 0</h3>';
    $sheep = new Animal("shaun");
    echo 'Name : '.$sheep->name.'<br>'; // "shaun"
    echo 'Legs :'.$sheep->legs.'<br>'; // 4
    echo 'Cold Blooded : '.$sheep->cold_blooded; // "no"

    echo '<h3>Release 1</h3>';
    $kodok = new Frog("buduk");
    echo 'Name : '.$kodok->name.'<br>'; 
    echo 'Legs :'.$kodok->legs.'<br>';
    echo 'Cold Blooded : '.$kodok->cold_blooded.'<br>';
    echo 'Jump :';
    $kodok->jump() ; 
    echo '<br><br>';

    $sungokong = new Ape("kera sakti");
    echo 'Name : '.$sungokong->name.'<br>'; 
    echo 'Legs :'.$sungokong->legs.'<br>';
    echo 'Cold Blooded : '.$sungokong->cold_blooded.'<br>';
    echo 'Yell :';
    $sungokong->yell();

    echo '<h3>Release 2</h3>';
    echo 'push ke repository masing masing';
    
?>